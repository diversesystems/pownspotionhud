package me.powns.potionhud.commands;

import me.powns.potionhud.PotionHudMod;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.client.IClientCommand;

public class GuiCommand extends CommandBase implements IClientCommand {
   private PotionHudMod mod;

   public GuiCommand(PotionHudMod mod) {
      this.mod = mod;
   }

   public boolean func_71519_b(ICommandSender sender) {
      return true;
   }

   public String getName() {
      return "potionhud";
   }

   public String getUsage(ICommandSender sender) {
      return "/" + this.getName();
   }

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		this.mod.openMainMenu();
	}

	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		return true;
	}

	@Override
	public boolean allowUsageWithoutPrefix(ICommandSender sender, String message) {
		return false;
	}
}
