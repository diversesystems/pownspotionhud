package me.powns.potionhud;

import me.powns.potionhud.commands.GuiCommand;
import me.powns.potionhud.gui.HudRenderer;
import me.powns.potionhud.gui.menu.MainMenu;
import me.powns.potionhud.settings.Settings;
import net.minecraft.client.Minecraft;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.RenderTickEvent;

@Mod(
   modid = "pownspotionhud",
   version = "1.0"
)
public class PotionHudMod {
   public static final String MODID = "pownspotionhud";
   public static final String VERSION = "1.0";
   private Settings settings = new Settings();
   private HudRenderer renderer = new HudRenderer(this);
   private boolean menuOpened;
   private Minecraft mc = Minecraft.getMinecraft();

   @EventHandler
   public void init(FMLInitializationEvent event) {
      FMLCommonHandler.instance().bus().register(this);
      FMLCommonHandler.instance().bus().register(this.renderer);
      ClientCommandHandler.instance.registerCommand(new GuiCommand(this));
      this.settings.loadConfig();
   }

   @SubscribeEvent
   public void onRenderTick(RenderTickEvent e) {
      if (this.menuOpened) {
         this.mc.displayGuiScreen(new MainMenu(this, this.settings));
         this.menuOpened = false;
      }

   }

   public HudRenderer getRenderer() {
      return this.renderer;
   }

   public void openMainMenu() {
      this.menuOpened = true;
   }

   public Settings getSettings() {
      return this.settings;
   }
}
