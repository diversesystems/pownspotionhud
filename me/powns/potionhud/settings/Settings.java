package me.powns.potionhud.settings;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.potion.Potion;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;

public class Settings {
   private boolean renderIcons = true;
   private boolean renderLevelsAbove4;
   private boolean showWhileChat;
   private boolean romanNumerals;
   private File saveFile;
   private ArrayList effectSettings = new ArrayList();
   private int opacity;
   private float scale;
   private int minX;
   private int minY;
   private int maxX;
   private int maxY;
   private int addX;
   private int addY;

   public Settings() {
      this.saveFile = new File(Minecraft.getMinecraft().mcDataDir, "config/PotionEffectHud.config");
      this.initializeSettings();
      this.opacity = 255;
      this.scale = 1.0F;
      this.renderLevelsAbove4 = true;
      this.romanNumerals = true;
      this.addX = 1;
      this.addY = 1;
   }

   public boolean contains(int mouseX, int mouseY) {
      return mouseX > this.minX && mouseX < this.maxX + 16 && mouseY > this.minY - Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT && mouseY < this.maxY;
   }

   public boolean isRenderIcons() {
      return this.renderIcons;
   }

   public void setRenderIcons(boolean renderIcons) {
      this.renderIcons = renderIcons;
   }

   public ArrayList getEffectSettings() {
      return this.effectSettings;
   }

   public ArrayList getEffectSettingsButTarget(int targetId) {
      ArrayList result = new ArrayList();
      Iterator var3 = this.effectSettings.iterator();

      while(var3.hasNext()) {
         EffectSetting s = (EffectSetting)var3.next();
         if (s.getEffectId() != targetId) {
            result.add(s);
         }
      }

      return result;
   }

   private void initializeSettings() {
      this.effectSettings.clear();
      this.effectSettings.add(new EffectSetting(1, this));
      this.effectSettings.add(new EffectSetting(2, this));
      this.effectSettings.add(new EffectSetting(3, this));
      this.effectSettings.add(new EffectSetting(4, this));
      this.effectSettings.add(new EffectSetting(5, this));
      this.effectSettings.add(new EffectSetting(8, this));
      this.effectSettings.add(new EffectSetting(9, this));
      this.effectSettings.add(new EffectSetting(10, this));
      this.effectSettings.add(new EffectSetting(11, this));
      this.effectSettings.add(new EffectSetting(12, this));
      this.effectSettings.add(new EffectSetting(13, this));
      this.effectSettings.add(new EffectSetting(14, this));
      this.effectSettings.add(new EffectSetting(15, this));
      this.effectSettings.add(new EffectSetting(16, this));
      this.effectSettings.add(new EffectSetting(17, this));
      this.effectSettings.add(new EffectSetting(18, this));
      this.effectSettings.add(new EffectSetting(19, this));
      this.effectSettings.add(new EffectSetting(20, this));
      this.effectSettings.add(new EffectSetting(21, this));
      this.effectSettings.add(new EffectSetting(22, this));
   }

   public EffectSetting getEffectSetting(int effectId) {
      Iterator var2 = this.effectSettings.iterator();

      EffectSetting s;
      do {
         if (!var2.hasNext()) {
            return null;
         }

         s = (EffectSetting)var2.next();
      } while(s.getEffectId() != effectId);

      return s;
   }

   public int getOpacity() {
      return this.opacity;
   }

   public void setOpacity(int opacity) {
      this.opacity = opacity;
   }

   public float getScale() {
      return this.scale;
   }

   public void setScale(float newScale) {
      this.scale = newScale;
   }

   public void saveConfig() {
      Configuration config = new Configuration(this.saveFile);
      this.updateConfig(config, false);
      config.save();
   }

   public void loadConfig() {
      Configuration config = new Configuration(this.saveFile);
      config.load();
      this.updateConfig(config, true);
   }

   public int getAddX() {
      return this.addX;
   }

   public void setAddX(int addX) {
      this.addX = addX;
   }

   public int getAddY() {
      return this.addY;
   }

   public void setAddY(int addY) {
      this.addY = addY;
   }

   public void setMinX(int minX) {
      this.minX = minX;
   }

   public void setMinY(int minY) {
      this.minY = minY;
   }

   public void setMaxX(int maxX) {
      this.maxX = maxX;
   }

   public void setMaxY(int maxY) {
      this.maxY = maxY;
   }

   public int getMinX() {
      return this.minX;
   }

   public int getMinY() {
      return this.minY;
   }

   public int getMaxX() {
      return this.maxX;
   }

   public int getMaxY() {
      return this.maxY;
   }

   public boolean isRenderLevelsAbove4() {
      return this.renderLevelsAbove4;
   }

   public void setRenderLevelsAbove4(boolean renderLevelsAbove4) {
      this.renderLevelsAbove4 = renderLevelsAbove4;
   }

   public boolean isShowWhileChat() {
      return this.showWhileChat;
   }

   public void setShowWhileChat(boolean showWhileChat) {
      this.showWhileChat = showWhileChat;
   }

   public boolean isRomanNumerals() {
      return this.romanNumerals;
   }

   public void setRomanNumerals(boolean romanNumerals) {
      this.romanNumerals = romanNumerals;
   }

   public void replaceSetting(EffectSetting oldSetting, EffectSetting replaceWith) {
      EffectSetting newSetting = replaceWith.getSyncCopy(oldSetting);
      ArrayList newSettings = new ArrayList();

      for(int i = 0; i < this.effectSettings.size(); ++i) {
         EffectSetting current = (EffectSetting)this.effectSettings.get(i);
         if (current.getEffectId() == oldSetting.getEffectId()) {
            newSettings.add(newSetting);
         } else {
            newSettings.add(current);
         }
      }

      this.effectSettings = newSettings;
   }

   private void updateConfig(Configuration config, boolean load) {
      Property prop = config.get("Global", "renderIcons", true);
      if (load) {
         this.renderIcons = prop.getBoolean();
      } else {
         prop.setValue(this.renderIcons);
      }

      prop = config.get("Global", "renderLevelsAbove4", true);
      if (load) {
         this.renderLevelsAbove4 = prop.getBoolean();
      } else {
         prop.setValue(this.renderLevelsAbove4);
      }

      prop = config.get("Global", "scale", 1.0D);
      if (load) {
         this.scale = (float)prop.getDouble();
      } else {
         prop.setValue((double)this.scale);
      }

      prop = config.get("Global", "showInChat", false);
      if (load) {
         this.showWhileChat = prop.getBoolean();
      } else {
         prop.setValue(this.showWhileChat);
      }

      prop = config.get("Global", "romanNumerals", true);
      if (load) {
         this.romanNumerals = prop.getBoolean();
      } else {
         prop.setValue(this.romanNumerals);
      }

      prop = config.get("Global", "posX", 0);
      if (load) {
         this.addX = prop.getInt();
      } else {
         prop.setValue(this.addX);
      }

      prop = config.get("Global", "posY", 0);
      if (load) {
         this.addY = prop.getInt();
      } else {
         prop.setValue(this.addY);
      }

      Iterator var4 = this.effectSettings.iterator();

      while(var4.hasNext()) {
         EffectSetting s = (EffectSetting)var4.next();
         Potion target = Potion.getPotionById(s.getEffectId());
         prop = config.get("" + s.getEffectId(), "enabled", true);
         if (load) {
            s.setEnabled(prop.getBoolean());
         } else {
            prop.setValue(s.isEnabled());
         }

         prop = config.get("" + s.getEffectId(), "chromaName", false);
         if (load) {
            s.setChromaName(prop.getBoolean());
         } else {
            prop.setValue(s.isChromaName());
         }

         prop = config.get("" + s.getEffectId(), "chromaTimer", false);
         if (load) {
            s.setChromaTimer(prop.getBoolean());
         } else {
            prop.setValue(s.isChromaTimer());
         }

         prop = config.get("" + s.getEffectId(), "nameColor", -1);
         if (load) {
            s.setNameColor(prop.getInt());
         } else {
            prop.setValue(s.getNameColor());
         }

         prop = config.get("" + s.getEffectId(), "timerColor", -1);
         if (load) {
            s.setTimerColor(prop.getInt());
         } else {
            prop.setValue(s.getTimerColor());
         }

         prop = config.get("" + s.getEffectId(), "boldText", false);
         if (load) {
            s.setBold(prop.getBoolean());
         } else {
            prop.setValue(s.isBold());
         }

         prop = config.get("" + s.getEffectId(), "italicText", false);
         if (load) {
            s.setItalic(prop.getBoolean());
         } else {
            prop.setValue(s.isItalic());
         }

         prop = config.get("" + s.getEffectId(), "underlinedText", false);
         if (load) {
            s.setUnderlined(prop.getBoolean());
         } else {
            prop.setValue(s.isUnderlined());
         }

         prop = config.get("" + s.getEffectId(), "flickerWhenLow", true);
         if (load) {
            s.setFlickerWhenLow(prop.getBoolean());
         } else {
            prop.setValue(s.isFlickerWhenLow());
         }

         prop = config.get("" + s.getEffectId(), "flickerStartTime", 10);
         if (load) {
            s.setFlickerStartTime(prop.getInt());
         } else {
            prop.setValue(s.getFlickerStartTime());
         }

         prop = config.get("" + s.getEffectId(), "flickerSpeed", 10);
         if (load) {
            s.setFlickerSpeed(prop.getInt());
         } else {
            prop.setValue(s.getFlickerSpeed());
         }

         prop = config.get("" + s.getEffectId(), "customName", I18n.format(target.getName(), new Object[0]));
         if (load) {
            s.setCustomName(prop.getString());
         } else {
            prop.setValue(s.getCustomName());
         }
      }

   }
}
