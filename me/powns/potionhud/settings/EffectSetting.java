package me.powns.potionhud.settings;

import net.minecraft.util.text.TextFormatting;

import java.awt.Color;

public class EffectSetting {
   private int timerColor;
   private int nameColor;
   private boolean chromaName;
   private boolean chromaTimer;
   private Settings settings;
   private int effectId;
   private boolean enabled;
   private boolean bold;
   private boolean italic;
   private boolean underlined;
   private boolean flickerWhenLow;
   private int flickerStartTime;
   private int flickerSpeed;
   private String customName;

   public EffectSetting(int effectId, Settings settings) {
      this.effectId = effectId;
      this.timerColor = -1;
      this.nameColor = -1;
      this.settings = settings;
      this.enabled = true;
      this.flickerWhenLow = true;
      this.flickerSpeed = 16;
      this.customName = "";
   }

   public String getNameAttachments() {
      String result = "";
      if (this.bold) {
         result = result + TextFormatting.BOLD;
      }

      if (this.italic) {
         result = result + TextFormatting.ITALIC;
      }

      if (this.underlined) {
         result = result + TextFormatting.UNDERLINE;
      }

      return result;
   }

   public boolean isChromaName() {
      return this.chromaName;
   }

   public void setChromaName(boolean chromaName) {
      this.chromaName = chromaName;
   }

   public boolean isChromaTimer() {
      return this.chromaTimer;
   }

   public void setChromaTimer(boolean chromaTimer) {
      this.chromaTimer = chromaTimer;
   }

   public int getTimerColor() {
      return this.chromaTimer ? Color.HSBtoRGB((float)(System.currentTimeMillis() % 1500L) / 1500.0F, 0.8F, 0.8F) : this.getColorWithAlpha(this.timerColor, this.settings.getOpacity());
   }

   public void setTimerColor(int timerColor) {
      this.timerColor = timerColor;
   }

   public int getNameColor() {
      return this.chromaName ? Color.HSBtoRGB((float)(System.currentTimeMillis() % 1500L) / 1500.0F, 0.8F, 0.8F) : this.getColorWithAlpha(this.nameColor, this.settings.getOpacity());
   }

   public void setNameColor(int nameColor) {
      this.nameColor = nameColor;
   }

   public boolean isEnabled() {
      return this.enabled;
   }

   public void setEnabled(boolean enabled) {
      this.enabled = enabled;
   }

   public int getEffectId() {
      return this.effectId;
   }

   public boolean isBold() {
      return this.bold;
   }

   public void setBold(boolean bold) {
      this.bold = bold;
   }

   public boolean isItalic() {
      return this.italic;
   }

   public void setItalic(boolean italic) {
      this.italic = italic;
   }

   public boolean isUnderlined() {
      return this.underlined;
   }

   public void setUnderlined(boolean underlined) {
      this.underlined = underlined;
   }

   public void setEffectId(int effectId) {
      this.effectId = effectId;
   }

   public boolean isFlickerWhenLow() {
      return this.flickerWhenLow;
   }

   public void setFlickerWhenLow(boolean flickerWhenLow) {
      this.flickerWhenLow = flickerWhenLow;
   }

   public int getFlickerStartTime() {
      return this.flickerStartTime;
   }

   public void setFlickerStartTime(int flickerStartTime) {
      this.flickerStartTime = flickerStartTime;
   }

   public int getFlickerSpeed() {
      return this.flickerSpeed;
   }

   public void setFlickerSpeed(int flickerSpeed) {
      this.flickerSpeed = flickerSpeed;
   }

   public String getCustomName() {
      return this.customName;
   }

   public void setCustomName(String customName) {
      this.customName = customName;
   }

   public EffectSetting getSyncCopy(EffectSetting syncTo) {
      EffectSetting original = this.settings.getEffectSetting(this.effectId);
      EffectSetting result = new EffectSetting(syncTo.getEffectId(), this.settings);
      result.setTimerColor(original.getTimerColor());
      result.setNameColor(original.getNameColor());
      result.setChromaName(original.isChromaName());
      result.setChromaTimer(original.isChromaTimer());
      result.setEnabled(syncTo.isEnabled());
      result.setBold(original.isBold());
      result.setItalic(original.isItalic());
      result.setUnderlined(original.isUnderlined());
      result.setFlickerWhenLow(original.isFlickerWhenLow());
      result.setFlickerStartTime(original.getFlickerStartTime());
      result.setFlickerSpeed(original.getFlickerSpeed());
      result.setCustomName(syncTo.getCustomName());
      return result;
   }

   private int getColorWithAlpha(int rgb, int a) {
      int r = rgb >> 16 & 255;
      int g = rgb >> 8 & 255;
      int b = rgb & 255;
      return a << 24 | r << 16 | g << 8 | b;
   }
}
