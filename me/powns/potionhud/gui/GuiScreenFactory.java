package me.powns.potionhud.gui;

import me.powns.potionhud.PotionHudMod;
import me.powns.potionhud.gui.components.GuiSidebar;
import me.powns.potionhud.gui.menu.effectsettings.EffectColorMenu;
import me.powns.potionhud.gui.menu.effectsettings.EffectFlickerMenu;
import me.powns.potionhud.gui.menu.effectsettings.EffectImportMenu;
import me.powns.potionhud.gui.menu.effectsettings.EffectNameMenu;
import me.powns.potionhud.settings.EffectSetting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

public class GuiScreenFactory {
   public static GuiScreen getScreenInstance(int type, PotionHudMod mod, EffectSetting setting, GuiSidebar sidebar) {
      switch(type) {
      case 0:
         return new EffectColorMenu(mod, setting, mod.getSettings(), sidebar);
      case 1:
         return new EffectNameMenu(mod, setting, mod.getSettings(), sidebar);
      case 2:
         return new EffectFlickerMenu(mod, setting, mod.getSettings(), sidebar);
      case 3:
         return new EffectImportMenu(mod, setting, mod.getSettings(), sidebar, false);
      default:
         return Minecraft.getMinecraft().currentScreen;
      }
   }
}
