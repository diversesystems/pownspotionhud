package me.powns.potionhud.gui;

public class ButtonYCoords {
   private int startY;
   private int endY;
   private String buttonTitle;

   public ButtonYCoords(int startY, int endY, String buttonTitle) {
      this.startY = startY;
      this.endY = endY;
      this.buttonTitle = buttonTitle;
   }

   public int getStartY() {
      return this.startY;
   }

   public int getEndY() {
      return this.endY;
   }

   public String getButtonTitle() {
      return this.buttonTitle;
   }
}
