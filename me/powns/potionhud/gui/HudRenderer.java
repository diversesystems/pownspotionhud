package me.powns.potionhud.gui;

import java.util.Iterator;
import me.powns.potionhud.PotionHudMod;
import me.powns.potionhud.gui.menu.MainMenu;
import me.powns.potionhud.gui.menu.effectsettings.EffectFlickerMenu;
import me.powns.potionhud.settings.EffectSetting;
import me.powns.potionhud.settings.Settings;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.resources.I18n;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.RenderTickEvent;
import org.lwjgl.opengl.GL11;

public class HudRenderer {
   private PotionHudMod mod;
   private Settings settings;
   private Minecraft mc;
   private double zLevel;

   public HudRenderer(PotionHudMod mod) {
      this.mod = mod;
      this.settings = mod.getSettings();
      this.mc = Minecraft.getMinecraft();
      this.zLevel = 1.0D;
   }

   @SubscribeEvent
   public void onHudRender(RenderTickEvent e) {
      if (this.mc.world != null) {
         if (this.mc.currentScreen instanceof MainMenu) {
            this.renderMenuHUD();
            return;
         }

         if (this.mc.currentScreen == null || this.settings.isShowWhileChat() && this.mc.currentScreen instanceof GuiChat) {
            this.renderIngameHUD();
         }
      }

   }

   public void renderSingleCustomCoords(EffectSetting setting, int x, int y) {
      int maxX = x + 110;
      int maxY = y + 30;
      this.renderDummyEffect(setting, x, y, maxX, maxY);
   }

   public void renderSingleCustomCoordsWithOtherSettings(EffectSetting setting, EffectSetting syncFrom, int x, int y) {
      int maxX = x + 110;
      int maxY = y + 30;
      this.renderDummyEffectWithOtherColors(setting, syncFrom, x, y, maxX, maxY);
   }

   private void renderMenuHUD() {
      int[] effects = new int[]{1, 12, 2, 14};
      int addToY = -22;
      GL11.glTranslatef((this.settings.getScale() - 1.0F) * (float)(-this.settings.getAddX()), (this.settings.getScale() - 1.0F) * (float)(-this.settings.getAddY()), 0.0F);
      GL11.glScalef(this.settings.getScale(), this.settings.getScale(), 1.0F);
      GlStateManager.color(1.0F, 1.0F, 1.0F, (float)this.settings.getOpacity() / 255.0F);
      Gui.drawRect(this.settings.getAddX() + 6, this.settings.getAddY() + 3, this.settings.getMaxX() + 16, this.settings.getMaxY(), this.getColorWithAlpha(8168374, 50));
      Gui.drawRect(this.settings.getAddX() + 5, this.settings.getAddY() + 2, this.settings.getAddX() + 6, this.settings.getAddY() + 12, this.getColorWithAlpha(8103095, 255));
      Gui.drawRect(this.settings.getAddX() + 5, this.settings.getAddY() + 2, this.settings.getAddX() + 15, this.settings.getAddY() + 3, this.getColorWithAlpha(8103095, 255));
      Gui.drawRect(this.settings.getMaxX() + 17, this.settings.getAddY() + 2, this.settings.getMaxX() + 7, this.settings.getAddY() + 3, this.getColorWithAlpha(8103095, 255));
      Gui.drawRect(this.settings.getMaxX() + 17, this.settings.getAddY() + 2, this.settings.getMaxX() + 16, this.settings.getAddY() + 12, this.getColorWithAlpha(8103095, 255));
      Gui.drawRect(this.settings.getAddX() + 5, this.settings.getMaxY(), this.settings.getAddX() + 15, this.settings.getMaxY() + 1, this.getColorWithAlpha(8103095, 255));
      Gui.drawRect(this.settings.getAddX() + 5, this.settings.getMaxY() + 1, this.settings.getAddX() + 6, this.settings.getMaxY() - 10, this.getColorWithAlpha(8103095, 255));
      Gui.drawRect(this.settings.getMaxX() + 17, this.settings.getMaxY(), this.settings.getMaxX() + 7, this.settings.getMaxY() + 1, this.getColorWithAlpha(8103095, 255));
      Gui.drawRect(this.settings.getMaxX() + 17, this.settings.getMaxY() + 1, this.settings.getMaxX() + 16, this.settings.getMaxY() - 10, this.getColorWithAlpha(8103095, 255));
      GlStateManager.color(1.0F, 1.0F, 1.0F, (float)this.settings.getOpacity() / 255.0F);
      GL11.glScalef(1.0F / this.settings.getScale(), 1.0F / this.settings.getScale(), 1.0F);
      GL11.glTranslatef((this.settings.getScale() - 1.0F) * (float)this.settings.getAddX(), (this.settings.getScale() - 1.0F) * (float)this.settings.getAddY(), 0.0F);
      int[] var3 = effects;
      int var4 = effects.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         int effect = var3[var5];
         addToY += 22;
         EffectSetting setting = this.settings.getEffectSetting(effect);
         int minX = this.settings.getAddX();
         int minY = this.settings.getAddY();
         int maxX = this.settings.getAddX() + 110;
         int maxY = this.settings.getAddY() + 30 + addToY;
         this.renderDummyEffect(setting, minX, minY + addToY, maxX, maxY);
         this.settings.setMinX(minX);
         this.settings.setMinY(minY);
         this.settings.setMaxX(maxX);
         this.settings.setMaxY(maxY);
      }

   }

   private void renderIngameHUD() {
      Iterator it = this.mc.player.getActivePotionEffects().iterator();
      int addToY = -22;

      while(true) {
         while(true) {
            PotionEffect pot;
            EffectSetting setting;
            do {
               if (!it.hasNext()) {
                  return;
               }

               addToY += 22;
               pot = (PotionEffect)it.next();
               setting = this.settings.getEffectSetting(Potion.getIdFromPotion(pot.getPotion()));
            } while(this.settings.getEffectSetting(Potion.getIdFromPotion(pot.getPotion())) == null);

            if (setting.isEnabled() && this.mc.player.isPotionActive(Potion.getPotionById(setting.getEffectId()))) {
               int minX = this.settings.getAddX();
               int minY = this.settings.getAddY() + addToY;
               int maxX = this.settings.getAddX() + 110;
               int maxY = this.settings.getAddY() + 30 + addToY;
               if (setting.isFlickerWhenLow() && pot.getDuration() / 20 <= setting.getFlickerStartTime()) {
                  int flickerSpeed = setting.getFlickerSpeed() * 55000;
                  if (System.currentTimeMillis() * 1000L / (long)flickerSpeed % 2L == 0L) {
                     GlStateManager.color(1.0F, 1.0F, 1.0F, (float)this.settings.getOpacity() / 255.0F);
                     this.renderEffect(setting, minX, minY, maxX, maxY);
                     GlStateManager.color(1.0F, 1.0F, 1.0F, (float)this.settings.getOpacity() / 255.0F);
                  }
               } else {
                  GlStateManager.color(1.0F, 1.0F, 1.0F, (float)this.settings.getOpacity() / 255.0F);
                  this.renderEffect(setting, minX, minY, maxX, maxY);
                  GlStateManager.color(1.0F, 1.0F, 1.0F, (float)this.settings.getOpacity() / 255.0F);
               }

               this.settings.setMinX(minX);
               this.settings.setMinY(minY);
               this.settings.setMaxX(maxX);
               this.settings.setMaxY(maxY);
            } else {
               addToY -= 20;
            }
         }
      }
   }

   private void renderDummyEffect(EffectSetting setting, int minX, int minY, int maxX, int maxY) {
      if (this.mc.currentScreen instanceof EffectFlickerMenu && setting.isFlickerWhenLow()) {
         int flickerSpeed = setting.getFlickerSpeed() * '훘';
         if (System.currentTimeMillis() * 1000L / (long)flickerSpeed % 2L == 0L) {
            if (this.settings.isRenderIcons()) {
               this.renderDummyIcon(setting, minX, minY, maxX, maxY);
            }

            this.renderDummyText(setting, minX, minY, maxX, maxY, 100);
         }
      } else {
         if (this.settings.isRenderIcons()) {
            this.renderDummyIcon(setting, minX, minY, maxX, maxY);
         }

         this.renderDummyText(setting, minX, minY, maxX, maxY, 660);
      }

   }

   private void renderDummyEffectWithOtherColors(EffectSetting setting, EffectSetting syncFrom, int minX, int minY, int maxX, int maxY) {
      if (this.settings.isRenderIcons()) {
         this.renderDummyIcon(setting, minX, minY, maxX, maxY);
      }

      this.renderDummyTextWithOtherColors(setting, syncFrom, minX, minY, maxX, maxY, 660);
   }

   private void renderEffect(EffectSetting setting, int minX, int minY, int maxX, int maxY) {
      if (this.settings.isRenderIcons()) {
         this.renderIcon(setting, minX, minY, maxX, maxY);
      }

      this.renderText(setting, minX, minY, maxX, maxY);
   }

   private void renderDummyIcon(EffectSetting setting, int minX, int minY, int maxX, int maxY) {
      Potion potion = Potion.getPotionById(setting.getEffectId());
      PotionEffect effect = new PotionEffect(potion, 30, 0, false, false);
      this.doRenderIcon(potion, effect, maxX, maxY, minX, minY);
   }

   private void renderIcon(EffectSetting setting, int minX, int minY, int maxX, int maxY) {
      Potion potion = Potion.getPotionById(setting.getEffectId());
      PotionEffect effect = this.mc.player.getActivePotionEffect(potion);
      this.doRenderIcon(potion, effect, maxX, maxY, minX, minY);
   }

   private void renderText(EffectSetting setting, int minX, int minY, int maxX, int maxY) {
      Potion potion = Potion.getPotionById(setting.getEffectId());
      PotionEffect effect = this.mc.player.getActivePotionEffect(potion);
      this.doRenderText(setting, potion, effect, maxX, maxY, minX, minY, setting.getTimerColor(), setting.getNameColor(), setting.getNameAttachments());
   }

   private void renderDummyText(EffectSetting setting, int minX, int minY, int maxX, int maxY, int effectTime) {
      Potion potion = Potion.getPotionById(setting.getEffectId());
      PotionEffect effect = new PotionEffect(potion, effectTime, 0, false, false);
      if (this.mc.currentScreen instanceof MainMenu && (Potion.getIdFromPotion(effect.getPotion()) == 1 || Potion.getIdFromPotion(effect.getPotion()) == 2)) {
         effect = new PotionEffect(potion, effectTime, 1, false, false);
      }

      this.doRenderText(setting, potion, effect, maxX, maxY, minX, minY, setting.getTimerColor(), setting.getNameColor(), setting.getNameAttachments());
   }

   private void renderDummyTextWithOtherColors(EffectSetting setting, EffectSetting syncFrom, int minX, int minY, int maxX, int maxY, int effectTime) {
      Potion potion = Potion.getPotionById(setting.getEffectId());
      PotionEffect effect = new PotionEffect(potion, effectTime, 0, false, false);
      this.doRenderText(setting, potion, effect, maxX, maxY, minX, minY, syncFrom.getTimerColor(), syncFrom.getNameColor(), syncFrom.getNameAttachments());
   }

   private void doRenderIcon(Potion potion, PotionEffect effect, int maxX, int maxY, int minX, int minY) {
      if (potion.shouldRenderInvText(effect)) {
         int x = this.shouldRenderMirrored() ? maxX - 10 : minX;
         int y = minY - 1;
         this.mc.getTextureManager().bindTexture(new ResourceLocation("textures/gui/container/inventory.png"));
         int i1 = potion.getStatusIconIndex();
         GlStateManager.pushMatrix();
         float opacity = (float)this.settings.getOpacity() / 255.0F;
         GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
         GL11.glTranslatef((this.settings.getScale() - 1.0F) * (float)(-this.settings.getAddX()), (this.settings.getScale() - 1.0F) * (float)(-this.settings.getAddY()), 0.0F);
         GL11.glScalef(this.settings.getScale(), this.settings.getScale(), 1.0F);
         this.drawTexturedModalRect((int)((double)x + 6.0D), (int)((double)y + 7.0D), i1 % 8 * 18, 198 + i1 / 8 * 18, 18, 18);
         GL11.glScalef(1.0F / this.settings.getScale(), 1.0F / this.settings.getScale(), 1.0F);
         GL11.glTranslatef((this.settings.getScale() - 1.0F) * (float)this.settings.getAddX(), (this.settings.getScale() - 1.0F) * (float)this.settings.getAddY(), 0.0F);
         GlStateManager.popMatrix();
      }
   }

   private void doRenderText(EffectSetting setting, Potion potion, PotionEffect effect, int maxX, int maxY, int minX, int minY, int timerColor, int nameColor, String nameAttachments) {
      if (potion.shouldRenderInvText(effect)) {
         String s1 = setting.getCustomName().isEmpty() ? I18n.format(potion.getName(), new Object[0]) : setting.getCustomName();
         if (this.settings.isRomanNumerals()) {
            if (effect.getAmplifier() == 1) {
               s1 = s1 + " " + I18n.format("enchantment.level.2", new Object[0]);
            } else if (effect.getAmplifier() == 2) {
               s1 = s1 + " " + I18n.format("enchantment.level.3", new Object[0]);
            } else if (effect.getAmplifier() == 3) {
               s1 = s1 + " " + I18n.format("enchantment.level.4", new Object[0]);
            } else if (effect.getAmplifier() > 3 && this.settings.isRenderLevelsAbove4()) {
               s1 = s1 + " " + (effect.getAmplifier() + 1);
            }
         } else {
            s1 = effect.getAmplifier() > 0 ? s1 + " " + (effect.getAmplifier() + 1) : s1;
         }

         String s2 = Potion.getPotionDurationString(effect,1.0f);
         int nameAttchmentsWidth = (setting.isBold() ? setting.getCustomName().length() : 0) + (setting.isItalic() ? 1 : 0);
         int nameX = this.shouldRenderMirrored() ? maxX - 34 - this.mc.fontRenderer.getStringWidth(s1) - nameAttchmentsWidth : minX + (this.settings.isRenderIcons() ? 0 : -20);
         int x = this.shouldRenderMirrored() ? maxX - 34 - this.mc.fontRenderer.getStringWidth(s2) : minX + (this.settings.isRenderIcons() ? 0 : -20);
         GlStateManager.pushMatrix();
         GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
         GL11.glTranslatef((this.settings.getScale() - 1.0F) * (float)(-this.settings.getAddX()), (this.settings.getScale() - 1.0F) * (float)(-this.settings.getAddY()), 0.0F);
         GL11.glScalef(this.settings.getScale(), this.settings.getScale(), 1.0F);
         this.mc.fontRenderer.drawStringWithShadow(nameAttachments + s1, (float)(nameX + 10 + 18), (float)(minY + 6), nameColor);
         int offset = this.mc.fontRenderer.FONT_HEIGHT + 1;
         this.mc.fontRenderer.drawStringWithShadow(s2, (float)(x + 10 + 18), (float)(minY + 6 + offset), timerColor);
         GL11.glScalef(1.0F / this.settings.getScale(), 1.0F / this.settings.getScale(), 1.0F);
         GL11.glTranslatef((this.settings.getScale() - 1.0F) * (float)this.settings.getAddX(), (this.settings.getScale() - 1.0F) * (float)this.settings.getAddY(), 0.0F);
         GlStateManager.popMatrix();
      }
   }

   private void drawTexturedModalRect(int x, int y, int textureX, int textureY, int width, int height) {
      float f = 0.00390625F;
      float f2 = 0.00390625F;
      Tessellator tessellator = Tessellator.getInstance();
      BufferBuilder worldrenderer = tessellator.getBuffer();
      worldrenderer.begin(7, DefaultVertexFormats.POSITION_TEX);
      worldrenderer.pos((double)(x + 0), (double)(y + height), 0.0D).tex((double)((float)(textureX + 0) * 0.00390625F), (double)((float)(textureY + height) * 0.00390625F)).endVertex();
      worldrenderer.pos((double)(x + width), (double)(y + height), 0.0D).tex((double)((float)(textureX + width) * 0.00390625F), (double)((float)(textureY + height) * 0.00390625F)).endVertex();
      worldrenderer.pos((double)(x + width), (double)(y + 0), 0.0D).tex((double)((float)(textureX + width) * 0.00390625F), (double)((float)(textureY + 0) * 0.00390625F)).endVertex();
      worldrenderer.pos((double)(x + 0), (double)(y + 0), 0.0D).tex((double)((float)(textureX + 0) * 0.00390625F), (double)((float)(textureY + 0) * 0.00390625F)).endVertex();
      tessellator.draw();
   }

   private int getColorWithAlpha(int rgb, int a) {
      int r = rgb >> 16 & 255;
      int g = rgb >> 8 & 255;
      int b = rgb & 255;
      return a << 24 | r << 16 | g << 8 | b;
   }

   private boolean shouldRenderMirrored() {
      ScaledResolution sr = new ScaledResolution(this.mc);
      boolean b1 = sr.getScaledWidth() / 2 <= this.settings.getMinX();
      boolean b2 = this.mc.currentScreen == null || this.mc.currentScreen instanceof MainMenu;
      return b1 && b2;
   }
}
