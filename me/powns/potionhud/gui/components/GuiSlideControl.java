package me.powns.potionhud.gui.components;

import java.text.DecimalFormat;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;

public class GuiSlideControl extends GuiButton {
   public String label;
   public float curValue;
   public float minValue;
   public float maxValue;
   public boolean isSliding;
   public boolean useIntegers;
   private static DecimalFormat numFormat = new DecimalFormat("#.00");

   public GuiSlideControl(int id, int x, int y, int width, int height, String displayString, float minVal, float maxVal, float curVal, boolean useInts) {
      super(id, x, y, width, height, useInts ? displayString + (int)curVal : displayString + numFormat.format((double)curVal));
      this.label = displayString;
      this.minValue = minVal;
      this.maxValue = maxVal;
      this.curValue = (curVal - minVal) / (maxVal - minVal);
      this.useIntegers = useInts;
   }

   public float GetValueAsFloat() {
      return (this.maxValue - this.minValue) * this.curValue + this.minValue;
   }

   public int GetValueAsInt() {
      return (int)((this.maxValue - this.minValue) * this.curValue + this.minValue);
   }

   protected float roundValue(float value) {
      value = 0.01F * (float)Math.round(value / 0.01F);
      return value;
   }

   public String GetLabel() {
      return this.useIntegers ? this.label + this.GetValueAsInt() : this.label + numFormat.format((double)this.GetValueAsFloat());
   }

   protected void SetLabel() {
      this.displayString = this.GetLabel();
   }

   public int getHoverState(boolean isMouseOver) {
      return 0;
   }

   protected void mouseDragged(Minecraft mc, int mousePosX, int mousePosY) {
      if (this.visible) {
         if (this.isSliding) {
            this.curValue = ((float)mousePosX - ((float)this.x + 4.0F)) / ((float)this.width - 8.0F);
            if (this.curValue < 0.0F) {
               this.curValue = 0.0F;
            }

            if (this.curValue > 1.0F) {
               this.curValue = 1.0F;
            }

            this.SetLabel();
         }

         GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
         this.drawTexturedModalRect(this.x + (int)(this.curValue * (float)(this.width - 8)), this.y, 0, 66, 4, 20);
         this.drawTexturedModalRect(this.x + (int)(this.curValue * (float)(this.width - 8)) + 4, this.y, 196, 66, 4, 20);
      }

   }

   public boolean mousePressed(Minecraft mc, int mousePosX, int mousePosY) {
      if (!super.mousePressed(mc, mousePosX, mousePosY)) {
         return false;
      } else {
         this.curValue = ((float)mousePosX - ((float)this.x + 4.0F)) / ((float)this.width - 8.0F);
         if (this.curValue < 0.0F) {
            this.curValue = 0.0F;
         }

         if (this.curValue > 1.0F) {
            this.curValue = 1.0F;
         }

         this.SetLabel();
         return this.isSliding ? (this.isSliding = false) : (this.isSliding = true);
      }
   }

   public void mouseReleased(int mousePosX, int mousePosY) {
      this.isSliding = false;
   }

   private int getColorWithAlpha(int rgb, int a) {
      int r = rgb >> 16 & 255;
      int g = rgb >> 8 & 255;
      int b = rgb & 255;
      return a << 24 | r << 16 | g << 8 | b;
   }
}
