package me.powns.potionhud.gui.components;

import java.util.ArrayList;
import me.powns.potionhud.PotionHudMod;
import me.powns.potionhud.settings.EffectSetting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.fml.client.GuiScrollingList;
import org.lwjgl.opengl.GL11;

public class GuiScrollerPane extends GuiScrollingList {
   private ArrayList items;
   private int selectedIndex = -1;
   private PotionHudMod mod;

   public GuiScrollerPane(Minecraft client, int width, int height, int top, int bottom, int left, int entryHeight, ArrayList items, PotionHudMod mod) {
      super(client, width, height, top, bottom, left, entryHeight);
      this.items = items;
      this.mod = mod;
   }

   public EffectSetting getSelected() {
      return this.selectedIndex != -1 ? (EffectSetting)this.items.get(this.selectedIndex) : null;
   }

   protected int getSize() {
      return this.items.size();
   }

   protected void elementClicked(int index, boolean doubleClick) {
      this.selectedIndex = index;
      this.isSelected(this.selectedIndex);
      if (doubleClick) {
         ((EffectSetting)this.items.get(index)).setEnabled(!((EffectSetting)this.items.get(index)).isEnabled());
      }

   }

   protected boolean isSelected(int index) {
      return index == this.selectedIndex;
   }

   protected void drawBackground() {
   }

   protected void drawSlot(int var1, int var2, int var3, int var4, Tessellator var5) {
      if (var3 > this.top && var3 + this.slotHeight <= this.bottom) {
         EffectSetting setting = (EffectSetting)this.items.get(var1);
         Potion potion = Potion.getPotionById(setting.getEffectId());
         PotionEffect effect = new PotionEffect(potion, 30, 0, false, false);
         this.mod.getRenderer().renderSingleCustomCoords(setting, var2 / 2 - 105, var3 - 5);
         this.renderEnabledTag(setting, var2, var3);
         GL11.glColor3f(255.0F, 255.0F, 255.0F);
      }

   }

   private void renderEnabledTag(EffectSetting setting, int x, int y) {
      Minecraft.getMinecraft().fontRenderer.drawString(setting.isEnabled() ? "Enabled" : "Disabled", (float)(x / 2 + 60), (float)(y + 6), setting.isEnabled() ? 52224 : 13382400, false);
   }

}
