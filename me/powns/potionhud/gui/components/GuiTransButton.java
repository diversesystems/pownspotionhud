package me.powns.potionhud.gui.components;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;

public class GuiTransButton extends GuiButton {
   public GuiTransButton(int id, int x, int y, int width, int height, String displayString) {
      super(id, x, y, width, height, displayString);
   }

   public void func_146112_a(Minecraft mc, int mouseX, int mouseY) {
      if (this.visible) {
         FontRenderer fontrenderer = Minecraft.getMinecraft().fontRenderer;
         Minecraft.getMinecraft().getTextureManager().bindTexture(BUTTON_TEXTURES);
         GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
         this.hovered = mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
         int i = this.getHoverState(this.hovered);
         GlStateManager.enableBlend();
         GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
         GlStateManager.blendFunc(770, 771);
         drawRect(this.x + 1, this.y + 1, this.x + this.width - 1, this.y + this.height - 1, this.getButtonColor());
         this.mouseDragged(mc, mouseX, mouseY);
         int l = 14737632;
         if (this.packedFGColour != 0) {
            l = this.packedFGColour;
         } else if (!this.enabled) {
            l = 10526880;
         } else if (this.hovered) {
            l = 16777120;
         }

         this.drawCenteredString(fontrenderer, this.displayString, this.x + this.width / 2, this.y + (this.height - 8) / 2, l);
      }

   }

   private int getButtonColor() {
      switch(this.getHoverState(this.hovered)) {
      case 0:
         return this.getColorWithAlpha(8168374, 50);
      case 1:
         return this.getColorWithAlpha(8168374, 100);
      case 2:
         return this.getColorWithAlpha(8168374, 150);
      default:
         return 0;
      }
   }

   private int getColorWithAlpha(int rgb, int a) {
      int r = rgb >> 16 & 255;
      int g = rgb >> 8 & 255;
      int b = rgb & 255;
      return a << 24 | r << 16 | g << 8 | b;
   }
}
