package me.powns.potionhud.gui.components;

import java.util.ArrayList;
import me.powns.potionhud.PotionHudMod;
import me.powns.potionhud.gui.ButtonYCoords;
import me.powns.potionhud.gui.GuiScreenFactory;
import me.powns.potionhud.settings.EffectSetting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;

public class GuiSidebar {
   public ArrayList pages = new ArrayList();
   public int x;
   public int y;
   public int height;
   public int width;
   public int slotHeight;
   public FontRenderer fontRenderer;
   public int sidebarColor;
   public int selectedIndex = 0;
   public int selectedColor;
   public int textColor;
   private int mouseX;
   private int mouseY;
   private float partialTicks;
   private PotionHudMod mod;
   private EffectSetting setting;
   private ArrayList buttons;

   public GuiSidebar(int x, int y, int height, int width, int slotHeight, FontRenderer fontRenderer, int sidebarColor, int selectedColor, int textColor, PotionHudMod mod, EffectSetting setting) {
      this.pages.add("Color Settings");
      this.pages.add("Name Settings");
      this.pages.add("Flickering Settings");
      this.pages.add("Import Settings");
      this.x = x;
      this.y = y;
      this.height = height;
      this.width = width;
      this.slotHeight = slotHeight;
      this.fontRenderer = fontRenderer;
      this.sidebarColor = sidebarColor;
      this.selectedColor = selectedColor;
      this.textColor = textColor;
      this.mod = mod;
      this.setting = setting;
   }

   public void setSetting(EffectSetting newSetting) {
      this.setting = newSetting;
   }

   public void drawSidebar(int mouseX, int mouseY, float partialTicks) {
      this.mouseX = mouseX;
      this.mouseY = mouseY;
      this.partialTicks = partialTicks;
      Gui.drawRect(this.x, this.y, this.width, this.height, this.getColorWithAlpha(this.sidebarColor, 50));
      this.drawPageButtons(this.pages.size() % 2 == 0);
   }

   public void onClick(int mouseX, int mouseY) {
      for(int i = 0; i < this.buttons.size(); ++i) {
         ButtonYCoords current = (ButtonYCoords)this.buttons.get(i);
         if (mouseY > current.getStartY() && mouseY < current.getEndY() && mouseX > this.x && mouseX < this.width) {
            this.selectedIndex = i;
            this.buttonClicked();
            return;
         }
      }

   }

   private void buttonClicked() {
      Minecraft.getMinecraft().displayGuiScreen(GuiScreenFactory.getScreenInstance(this.selectedIndex, this.mod, this.setting, this));
   }

   private void drawPageButtons(boolean evenAmount) {
      this.buttons = new ArrayList();
      int startHeight = 0;
      if (evenAmount) {
         startHeight = this.height / 2 - this.pages.size() / 2 * this.slotHeight;
      } else {
         startHeight = this.height / 2 - this.pages.size() / 2 * this.slotHeight + this.slotHeight / 2;
      }

      this.buttons = this.drawButtons(startHeight);
   }

   private ArrayList drawButtons(int startHeight) {
      ArrayList result = new ArrayList();

      for(int i = 0; i < this.pages.size(); ++i) {
         int y = startHeight + this.slotHeight * i;
         if (i == this.selectedIndex) {
            Gui.drawRect(this.x, y, this.width, y + this.slotHeight, this.getColorWithAlpha(this.selectedColor, 255));
         }

         this.fontRenderer.drawStringWithShadow((String)this.pages.get(i), (float)(this.x + 5), (float)(y + this.slotHeight / 2 - this.fontRenderer.FONT_HEIGHT / 2), this.textColor);
         result.add(new ButtonYCoords(y, y + this.slotHeight, (String)this.pages.get(i)));
      }

      return result;
   }

   private int getColorWithAlpha(int rgb, int a) {
      int r = rgb >> 16 & 255;
      int g = rgb >> 8 & 255;
      int b = rgb & 255;
      return a << 24 | r << 16 | g << 8 | b;
   }
}
