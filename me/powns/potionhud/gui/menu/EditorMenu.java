package me.powns.potionhud.gui.menu;

import java.io.IOException;
import me.powns.potionhud.PotionHudMod;
import me.powns.potionhud.gui.components.GuiScrollerPane;
import me.powns.potionhud.gui.components.GuiSidebar;
import me.powns.potionhud.gui.components.GuiTransButton;
import me.powns.potionhud.gui.menu.effectsettings.EffectColorMenu;
import me.powns.potionhud.settings.Settings;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import org.lwjgl.opengl.GL11;

public class EditorMenu extends GuiScreen {
   private PotionHudMod mod;
   private Settings s;
   private GuiScrollerPane list;
   private GuiButton editButton;
   private GuiButton toggleButton;

   public EditorMenu(PotionHudMod mod, Settings s) {
      this.mod = mod;
      this.s = s;
   }

   public void initGui() {
      this.list = new GuiScrollerPane(Minecraft.getMinecraft(), this.width, 200, 50, this.height - 70, 0, 25, this.s.getEffectSettings(), this.mod);
      this.buttonList.add(this.editButton = new GuiTransButton(0, this.getCenter() - 105, this.height - 60, 100, 20, "Edit"));
      this.buttonList.add(this.toggleButton = new GuiTransButton(1, this.getCenter() + 5, this.height - 60, 100, 20, "Toggle on/off"));
      this.buttonList.add(new GuiTransButton(2, this.getCenter() - 105, this.height - 30, 210, 20, "Return"));
      this.list.registerScrollButtons(null,4 ,5);
   }

   public int getRowPos(int rowNumber) {
      return this.height / 4 + (24 * rowNumber - 24) - 16;
   }

   public int getCenter() {
      return this.width / 2;
   }

   public void drawScreen(int mouseX, int mouseY, float partialTicks) {
      super.drawDefaultBackground();
      super.drawScreen(mouseX, mouseY, partialTicks);
      this.list.drawScreen(mouseX, mouseY, partialTicks);
      GL11.glPushMatrix();
      GL11.glScaled(2.0D, 2.0D, 2.0D);
      this.drawCenteredString(this.mc.fontRenderer, "StatusEffect Editor", this.getCenter() / 2, 10, -1);
      GL11.glPopMatrix();
      this.editButton.enabled = this.list.getSelected() != null;
      this.toggleButton.enabled = this.list.getSelected() != null;
   }

   protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
      super.mouseClicked(mouseX, mouseY, mouseButton);
   }

   protected void mouseReleased(int mouseX, int mouseY, int state) {
      super.mouseReleased(mouseX, mouseY, state);
   }

   protected void actionPerformed(GuiButton button) {
      switch(button.id) {
      case 0:
         this.mc.displayGuiScreen(new EffectColorMenu(this.mod, this.list.getSelected(), this.mod.getSettings(), (GuiSidebar)null));
         break;
      case 1:
         this.list.getSelected().setEnabled(!this.list.getSelected().isEnabled());
         break;
      case 2:
         this.mc.displayGuiScreen(new MainMenu(this.mod, this.s));
      }

   }

   public void onGuiClosed() {
      this.s.saveConfig();
   }

   public boolean doesGuiPauseGame() {
      return false;
   }
}
