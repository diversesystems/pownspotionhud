package me.powns.potionhud.gui.menu.effectsettings;

import java.io.IOException;
import me.powns.potionhud.PotionHudMod;
import me.powns.potionhud.gui.components.GuiSidebar;
import me.powns.potionhud.gui.components.GuiSlideControl;
import me.powns.potionhud.gui.components.GuiTransButton;
import me.powns.potionhud.gui.components.GuiTransTextField;
import me.powns.potionhud.gui.menu.EditorMenu;
import me.powns.potionhud.settings.EffectSetting;
import me.powns.potionhud.settings.Settings;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

import net.minecraft.util.text.TextFormatting;
import org.lwjgl.opengl.GL11;

public class EffectFlickerMenu extends GuiScreen {
   private PotionHudMod mod;
   private EffectSetting setting;
   private Settings s;
   private GuiSlideControl sliderFlickerSpeed;
   private GuiTransTextField timeText;
   private GuiSidebar sidebar;

   public EffectFlickerMenu(PotionHudMod mod, EffectSetting setting, Settings s, GuiSidebar sidebar) {
      this.mod = mod;
      this.s = s;
      this.setting = setting;
      this.sidebar = sidebar;
   }

   public void initGui() {
      if (this.sidebar == null) {
         this.sidebar = new GuiSidebar(0, 0, this.height, this.width / 5 + 20, 25, this.fontRenderer, 8168374, 8103095, -1, this.mod, this.setting);
      }

      this.sidebar.height = this.height;
      this.sidebar.width = this.width / 5 + 20;
      this.timeText = new GuiTransTextField(this.fontRenderer, this.getCenter() - 18, this.getRowPos(4), 40, 20);
      this.timeText.setMaxStringLength(23);
      this.timeText.setText("" + this.setting.getFlickerStartTime());
      this.timeText.setFocused(false);
      this.buttonList.add(this.sliderFlickerSpeed = new GuiSlideControl(13, this.getCenter() - 75, this.getRowPos(5), 150, 20, "Flicker time: ", 10.0F, 20.0F, (float)this.setting.getFlickerSpeed(), true));
      this.buttonList.add(new GuiTransButton(12, this.getCenter() - 75, this.getRowPos(3), 150, 20, (this.setting.isFlickerWhenLow() ? TextFormatting.GREEN : TextFormatting.RED) + "Low duration flickering"));
      this.buttonList.add(new GuiTransButton(5, this.getCenter() - 75, this.getRowPos(8), 150, 20, "Save"));
      this.sliderFlickerSpeed.visible = this.setting.isFlickerWhenLow();
   }

   public int getRowPos(int rowNumber) {
      return this.height / 4 + (24 * rowNumber - 24) - 16;
   }

   public int getCenter() {
      return (this.width + this.sidebar.width) / 2;
   }

   public void drawScreen(int mouseX, int mouseY, float partialTicks) {
      super.drawDefaultBackground();
      this.sidebar.drawSidebar(mouseX, mouseY, partialTicks);
      GL11.glColor3f(255.0F, 255.0F, 255.0F);
      this.mod.getRenderer().renderSingleCustomCoords(this.setting, this.getCenter() - 55 + 16, this.getRowPos(-1) + 10);
      this.drawCenteredString(this.mc.fontRenderer, "Flickering Settings", this.getCenter(), this.getRowPos(2), this.setting.getNameColor());
      if (this.setting.isFlickerWhenLow()) {
         this.timeText.drawTextBox();
         this.drawString(this.mc.fontRenderer, "Starts at", this.getCenter() - 68, this.getRowPos(4) + this.mc.fontRenderer.FONT_HEIGHT / 2 + 1, -1);
         this.drawString(this.mc.fontRenderer, "seconds", this.getCenter() + 27, this.getRowPos(4) + this.mc.fontRenderer.FONT_HEIGHT / 2 + 1, -1);
      }

      this.updateSettings();
      super.drawScreen(mouseX, mouseY, partialTicks);
   }

   protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
      super.mouseClicked(mouseX, mouseY, mouseButton);
      GuiTransTextField target = this.timeText;
      if (mouseX > target.xPosition && mouseX < target.xPosition + target.width && mouseY > target.yPosition && mouseY < target.yPosition + target.height && this.timeText.getVisible()) {
         this.timeText.mouseClicked(mouseX, mouseY, mouseButton);
      } else {
         this.timeText.setFocused(false);
      }

      if (mouseButton == 0) {
         this.sidebar.onClick(mouseX, mouseY);
      }

   }

   protected void mouseReleased(int mouseX, int mouseY, int state) {
      super.mouseReleased(mouseX, mouseY, state);
      this.sliderFlickerSpeed.isSliding = false;
   }

   protected void actionPerformed(GuiButton button) {
      switch(button.id) {
      case 5:
         this.mc.displayGuiScreen(new EditorMenu(this.mod, this.s));
         break;
      case 12:
         this.setting.setFlickerWhenLow(!this.setting.isFlickerWhenLow());
         button.displayString = (this.setting.isFlickerWhenLow() ? TextFormatting.GREEN : TextFormatting.RED) + "Low duration flickering";
         this.sliderFlickerSpeed.visible = this.setting.isFlickerWhenLow();
      }

   }

   protected void keyTyped(char par1, int par2) throws IOException {
      super.keyTyped(par1, par2);

      try {
         Integer.parseInt("" + par1);
         this.timeText.textboxKeyTyped(par1, par2);
      } catch (Exception var4) {
         if (("" + par1).equals("\b")) {
            this.timeText.textboxKeyTyped(par1, par2);
         }
      }

   }

   public void updateScreen() {
      super.updateScreen();
      this.timeText.updateCursorCounter();
   }

   private void updateSettings() {
      this.setting.setFlickerSpeed(this.sliderFlickerSpeed.GetValueAsInt());
      if (!this.timeText.getText().isEmpty()) {
         this.setting.setFlickerStartTime(Integer.parseInt(this.timeText.getText()));
      } else {
         this.setting.setFlickerStartTime(0);
      }

   }

   public void onGuiClosed() {
      this.s.saveConfig();
   }

   public boolean doesGuiPauseGame() {
      return false;
   }
}
