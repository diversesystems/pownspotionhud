package me.powns.potionhud.gui.menu.effectsettings;

import java.io.IOException;
import me.powns.potionhud.PotionHudMod;
import me.powns.potionhud.gui.components.GuiSidebar;
import me.powns.potionhud.gui.components.GuiTransButton;
import me.powns.potionhud.gui.components.GuiTransTextField;
import me.powns.potionhud.gui.menu.EditorMenu;
import me.powns.potionhud.settings.EffectSetting;
import me.powns.potionhud.settings.Settings;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraft.potion.Potion;
import net.minecraft.util.text.TextFormatting;
import org.lwjgl.opengl.GL11;

public class EffectNameMenu extends GuiScreen {
   private PotionHudMod mod;
   private EffectSetting setting;
   private Settings s;
   private GuiSidebar sidebar;
   private Potion target;
   private GuiTransTextField name;

   public EffectNameMenu(PotionHudMod mod, EffectSetting setting, Settings s, GuiSidebar sidebar) {
      this.mod = mod;
      this.s = s;
      this.setting = setting;
      this.sidebar = sidebar;
      this.target = Potion.getPotionById(setting.getEffectId());
   }

   public void initGui() {
      if (this.sidebar == null) {
         this.sidebar = new GuiSidebar(0, 0, this.height, this.width / 5 + 20, 25, this.fontRenderer, 8168374, 8103095, -1, this.mod, this.setting);
      }

      this.sidebar.height = this.height;
      this.sidebar.width = this.width / 5 + 20;
      this.name = new GuiTransTextField(this.fontRenderer, this.getCenter() - 25, this.getRowPos(3), 120, 20);
      this.name.setMaxStringLength(300);
      this.name.setText(this.setting.getCustomName());
      this.name.setFocused(false);
      this.buttonList.add(new GuiTransButton(6, this.getCenter() - 30, this.getRowPos(4), 60, 20, (this.setting.isBold() ? TextFormatting.GREEN : TextFormatting.RED) + "" + TextFormatting.BOLD + "Bold"));
      this.buttonList.add(new GuiTransButton(7, this.getCenter() - 95, this.getRowPos(4), 60, 20, (this.setting.isItalic() ? TextFormatting.GREEN : TextFormatting.RED) + "" + TextFormatting.ITALIC + "Italic"));
      this.buttonList.add(new GuiTransButton(8, this.getCenter() + 35, this.getRowPos(4), 60, 20, (this.setting.isUnderlined() ? TextFormatting.GREEN : TextFormatting.RED) + "" + TextFormatting.UNDERLINE + "Underlined"));
      this.buttonList.add(new GuiTransButton(9, this.getCenter() - 95, this.getRowPos(5), 190, 20, "Reset name"));
      this.buttonList.add(new GuiTransButton(5, this.getCenter() - 75, this.getRowPos(8), 150, 20, "Save"));
   }

   public int getRowPos(int rowNumber) {
      return this.height / 4 + (24 * rowNumber - 24) - 16;
   }

   public int getCenter() {
      return (this.width + this.sidebar.width) / 2;
   }

   public void drawScreen(int mouseX, int mouseY, float partialTicks) {
      super.drawDefaultBackground();
      this.sidebar.drawSidebar(mouseX, mouseY, partialTicks);
      this.name.drawTextBox();
      GL11.glColor3f(255.0F, 255.0F, 255.0F);
      this.mod.getRenderer().renderSingleCustomCoords(this.setting, this.getCenter() - 55 + 16, this.getRowPos(-1) + 10);
      this.drawCenteredString(this.mc.fontRenderer, this.setting.getNameAttachments() + "Name Settings", this.getCenter(), this.getRowPos(2), this.setting.getNameColor());
      this.drawString(this.mc.fontRenderer, "Custom Name: ", this.getCenter() - 94, this.getRowPos(3) + this.fontRenderer.FONT_HEIGHT / 2 + 2, -1);
      this.updateSettings();
      super.drawScreen(mouseX, mouseY, partialTicks);
   }

   protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
      super.mouseClicked(mouseX, mouseY, mouseButton);
      GuiTransTextField target = this.name;
      if (mouseX > target.xPosition && mouseX < target.xPosition + target.width && mouseY > target.yPosition && mouseY < target.yPosition + target.height && this.name.getVisible()) {
         this.name.mouseClicked(mouseX, mouseY, mouseButton);
      } else {
         this.name.setFocused(false);
      }

      if (mouseButton == 0) {
         this.sidebar.onClick(mouseX, mouseY);
      }

   }

   protected void keyTyped(char par1, int par2) throws IOException {
      super.keyTyped(par1, par2);
      this.name.textboxKeyTyped(par1, par2);
   }

   public void updateScreen() {
      super.updateScreen();
      this.name.updateCursorCounter();
   }

   protected void mouseReleased(int mouseX, int mouseY, int state) {
      super.mouseReleased(mouseX, mouseY, state);
   }

   protected void actionPerformed(GuiButton button) {
      switch(button.id) {
      case 5:
         this.mc.displayGuiScreen(new EditorMenu(this.mod, this.s));
         break;
      case 6:
         this.setting.setBold(!this.setting.isBold());
         button.displayString = (this.setting.isBold() ? TextFormatting.GREEN : TextFormatting.RED) + "" + TextFormatting.BOLD + "Bold";
         break;
      case 7:
         this.setting.setItalic(!this.setting.isItalic());
         button.displayString = (this.setting.isItalic() ? TextFormatting.GREEN : TextFormatting.RED) + "" + TextFormatting.ITALIC + "Italic";
         break;
      case 8:
         this.setting.setUnderlined(!this.setting.isUnderlined());
         button.displayString = (this.setting.isUnderlined() ? TextFormatting.GREEN : TextFormatting.RED) + "" + TextFormatting.UNDERLINE + "Underlined";
         break;
      case 9:
         this.name.setText(I18n.format(this.target.getName(), new Object[0]));
      }

   }

   public void onGuiClosed() {
      this.s.saveConfig();
   }

   public boolean doesGuiPauseGame() {
      return false;
   }

   private void updateSettings() {
      this.setting.setCustomName(this.name.getText());
   }
}
