package me.powns.potionhud.gui.menu.effectsettings;

import java.io.IOException;
import java.util.ArrayList;
import me.powns.potionhud.PotionHudMod;
import me.powns.potionhud.gui.components.GuiSidebar;
import me.powns.potionhud.gui.components.GuiTransButton;
import me.powns.potionhud.gui.components.GuiTransTextField;
import me.powns.potionhud.gui.menu.EditorMenu;
import me.powns.potionhud.settings.EffectSetting;
import me.powns.potionhud.settings.Settings;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.potion.Potion;
import net.minecraft.util.text.TextFormatting;
import org.lwjgl.opengl.GL11;

public class EffectImportMenu extends GuiScreen {
   private PotionHudMod mod;
   private EffectSetting setting;
   private Settings s;
   private GuiSidebar sidebar;
   private Potion target;
   private GuiTransTextField name;
   private ArrayList settingsExceptTarget;
   private int indexInList;
   private GuiButton next;
   private GuiButton prev;
   private GuiButton confirm;
   private boolean confirmed;

   public EffectImportMenu(PotionHudMod mod, EffectSetting setting, Settings s, GuiSidebar sidebar, boolean confirmed) {
      this.mod = mod;
      this.s = s;
      this.setting = setting;
      this.sidebar = sidebar;
      this.target = Potion.getPotionById(setting.getEffectId());
      this.settingsExceptTarget = s.getEffectSettingsButTarget(setting.getEffectId());
      this.indexInList = 0;
      this.confirmed = confirmed;
   }

   public void initGui() {
      if (this.sidebar == null) {
         this.sidebar = new GuiSidebar(0, 0, this.height, this.width / 5 + 20, 25, this.fontRenderer, 8168374, 8103095, -1, this.mod, this.setting);
      }

      this.sidebar.height = this.height;
      this.sidebar.width = this.width / 5 + 20;
      this.buttonList.add(this.next = new GuiTransButton(10, this.getCenter() - 80, this.getRowPos(4), 20, 20, "<"));
      this.buttonList.add(this.prev = new GuiTransButton(20, this.getCenter() + 60, this.getRowPos(4), 20, 20, ">"));
      this.buttonList.add(this.prev = new GuiTransButton(30, this.getCenter() - 70, this.getRowPos(5) + 5, 140, 20, "Confirm sync"));
      this.buttonList.add(new GuiTransButton(5, this.getCenter() - 75, this.getRowPos(8), 150, 20, "Save"));
   }

   public int getRowPos(int rowNumber) {
      return this.height / 4 + (24 * rowNumber - 24) - 16;
   }

   public int getCenter() {
      return (this.width + this.sidebar.width) / 2;
   }

   public void drawScreen(int mouseX, int mouseY, float partialTicks) {
      super.drawDefaultBackground();
      this.sidebar.drawSidebar(mouseX, mouseY, partialTicks);
      EffectSetting syncExample = (EffectSetting)this.settingsExceptTarget.get(this.indexInList);
      int syncExampleWidth = this.fontRenderer.getStringWidth(syncExample.getCustomName()) + 36 + (syncExample.isBold() ? 14 : 0);
      GL11.glColor3f(255.0F, 255.0F, 255.0F);
      this.mod.getRenderer().renderSingleCustomCoords(this.setting, this.getCenter() - 55 - 44, this.getRowPos(2));
      this.mod.getRenderer().renderSingleCustomCoordsWithOtherSettings(this.setting, syncExample, this.getCenter() - 55 + 87, this.getRowPos(2));
      this.mod.getRenderer().renderSingleCustomCoords(syncExample, this.getCenter() - syncExampleWidth / 2, this.getRowPos(4) - 15 + 10);
      this.drawCenteredString(this.mc.fontRenderer, this.setting.getNameAttachments() + "Import Settings", this.getCenter(), this.getRowPos(1), this.setting.getNameColor());
      this.drawCenteredString(this.mc.fontRenderer, TextFormatting.STRIKETHROUGH + "--" + TextFormatting.RESET + ">", this.getCenter(), this.getRowPos(2) + 10, -1);
      super.drawScreen(mouseX, mouseY, partialTicks);
   }

   protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
      super.mouseClicked(mouseX, mouseY, mouseButton);
      if (mouseButton == 0) {
         this.sidebar.onClick(mouseX, mouseY);
      }

   }

   protected void mouseReleased(int mouseX, int mouseY, int state) {
      super.mouseReleased(mouseX, mouseY, state);
   }

   protected void actionPerformed(GuiButton button) {
      switch(button.id) {
      case 5:
         this.mc.displayGuiScreen(new EditorMenu(this.mod, this.s));
         break;
      case 10:
         if (this.confirmed) {
            this.confirmed = false;
         }

         if (this.indexInList == 0) {
            this.indexInList = this.settingsExceptTarget.size() - 1;
         } else {
            --this.indexInList;
         }
         break;
      case 20:
         if (this.confirmed) {
            this.confirmed = false;
         }

         if (this.indexInList == this.settingsExceptTarget.size() - 1) {
            this.indexInList = 0;
         } else {
            ++this.indexInList;
         }
         break;
      case 30:
         this.s.replaceSetting(this.setting, (EffectSetting)this.settingsExceptTarget.get(this.indexInList));
         this.confirmed = true;
         this.sidebar.setSetting(this.s.getEffectSetting(this.setting.getEffectId()));
         this.mc.displayGuiScreen(new EffectImportMenu(this.mod, this.s.getEffectSetting(this.setting.getEffectId()), this.s, this.sidebar, this.confirmed));
      }

   }

   public void onGuiClosed() {
      this.s.saveConfig();
   }

   public boolean doesGuiPauseGame() {
      return false;
   }
}
