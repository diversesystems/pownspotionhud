package me.powns.potionhud.gui.menu.effectsettings;

import java.io.IOException;
import java.util.Iterator;
import me.powns.potionhud.PotionHudMod;
import me.powns.potionhud.gui.components.GuiSidebar;
import me.powns.potionhud.gui.components.GuiSlideControl;
import me.powns.potionhud.gui.components.GuiTransButton;
import me.powns.potionhud.gui.menu.EditorMenu;
import me.powns.potionhud.settings.EffectSetting;
import me.powns.potionhud.settings.Settings;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

import net.minecraft.util.text.TextFormatting;
import org.lwjgl.opengl.GL11;

public class EffectColorMenu extends GuiScreen {
   private PotionHudMod mod;
   private EffectSetting setting;
   private Settings s;
   private GuiSlideControl sliderRed;
   private GuiSlideControl sliderGreen;
   private GuiSlideControl sliderBlue;
   private GuiSlideControl sliderRed2;
   private GuiSlideControl sliderGreen2;
   private GuiSlideControl sliderBlue2;
   private GuiSidebar sidebar;

   public EffectColorMenu(PotionHudMod mod, EffectSetting setting, Settings s, GuiSidebar sidebar) {
      this.mod = mod;
      this.s = s;
      this.setting = setting;
      this.sidebar = sidebar;
   }

   public void initGui() {
      if (this.sidebar == null) {
         this.sidebar = new GuiSidebar(0, 0, this.height, this.width / 5 + 20, 25, this.fontRenderer, 8168374, 8103095, -1, this.mod, this.setting);
      }

      this.sidebar.height = this.height;
      this.sidebar.width = this.width / 5 + 20;
      this.buttonList.add(this.sliderRed = new GuiSlideControl(1, this.getCenter() - 125, this.getRowPos(3), 120, 20, "Red: ", 0.0F, 255.0F, (float)(this.setting.getNameColor() >> 16 & 255), true));
      this.buttonList.add(this.sliderGreen = new GuiSlideControl(2, this.getCenter() - 125, this.getRowPos(4), 120, 20, "Green: ", 0.0F, 255.0F, (float)(this.setting.getNameColor() >> 8 & 255), true));
      this.buttonList.add(this.sliderBlue = new GuiSlideControl(3, this.getCenter() - 125, this.getRowPos(5), 120, 20, "Blue: ", 0.0F, 255.0F, (float)(this.setting.getNameColor() & 255), true));
      this.buttonList.add(new GuiButton(40, this.getCenter() - 125, this.getRowPos(6), 120, 20, (this.setting.isChromaName() ? TextFormatting.GREEN : TextFormatting.RED) + "Chroma name"));
      this.buttonList.add(this.sliderRed2 = new GuiSlideControl(9, this.getCenter() + 5, this.getRowPos(3), 120, 20, "Red: ", 0.0F, 255.0F, (float)(this.setting.getTimerColor() >> 16 & 255), true));
      this.buttonList.add(this.sliderGreen2 = new GuiSlideControl(10, this.getCenter() + 5, this.getRowPos(4), 120, 20, "Green: ", 0.0F, 255.0F, (float)(this.setting.getTimerColor() >> 8 & 255), true));
      this.buttonList.add(this.sliderBlue2 = new GuiSlideControl(11, this.getCenter() + 5, this.getRowPos(5), 120, 20, "Blue: ", 0.0F, 255.0F, (float)(this.setting.getTimerColor() & 255), true));
      this.buttonList.add(new GuiButton(120, this.getCenter() + 5, this.getRowPos(6), 120, 20, (this.setting.isChromaTimer() ? TextFormatting.GREEN : TextFormatting.RED) + "Chroma timer"));
      this.buttonList.add(new GuiTransButton(5, this.getCenter() - 75, this.getRowPos(8), 150, 20, "Save"));
   }

   public int getRowPos(int rowNumber) {
      return this.height / 4 + (24 * rowNumber - 24) - 16;
   }

   public int getCenter() {
      return (this.width + this.sidebar.width) / 2;
   }

   protected void mouseClicked(int mouseX, int mouseY, int which) throws IOException {
      super.mouseClicked(mouseX, mouseY, which);
      if (which == 0) {
         this.sidebar.onClick(mouseX, mouseY);
      }

   }

   protected void mouseReleased(int mouseX, int mouseY, int which) {
      if (which == 0 || which == 1) {
         Iterator var4 = this.buttonList.iterator();

         while(var4.hasNext()) {
            Object slider = var4.next();
            if (slider instanceof GuiSlideControl) {
               ((GuiSlideControl)slider).isSliding = false;
            }
         }
      }

   }

   public void drawScreen(int mouseX, int mouseY, float partialTicks) {
      super.drawDefaultBackground();
      this.sidebar.drawSidebar(mouseX, mouseY, partialTicks);
      super.drawScreen(mouseX, mouseY, partialTicks);
      GL11.glColor3f(255.0F, 255.0F, 255.0F);
      this.mod.getRenderer().renderSingleCustomCoords(this.setting, this.getCenter() - 55 + 16, this.getRowPos(-1) + 10);
      this.drawCenteredString(this.mc.fontRenderer, "Name Color", this.getCenter() - 65, this.getRowPos(2), this.setting.getNameColor());
      this.drawCenteredString(this.mc.fontRenderer, "Timer Color", this.getCenter() + 65, this.getRowPos(2), this.setting.getTimerColor());
      this.updateSettings();
   }

   protected void actionPerformed(GuiButton button) {
      switch(button.id) {
      case 5:
         this.mc.displayGuiScreen(new EditorMenu(this.mod, this.s));
         break;
      case 6:
         this.setting.setBold(!this.setting.isBold());
         button.displayString = (this.setting.isBold() ? TextFormatting.GREEN : TextFormatting.RED) + "" + TextFormatting.BOLD + "Bold";
         break;
      case 7:
         this.setting.setItalic(!this.setting.isItalic());
         button.displayString = (this.setting.isItalic() ? TextFormatting.GREEN : TextFormatting.RED) + "" + TextFormatting.ITALIC + "Italic";
         break;
      case 8:
         this.setting.setUnderlined(!this.setting.isUnderlined());
         button.displayString = (this.setting.isUnderlined() ? TextFormatting.GREEN : TextFormatting.RED) + "" + TextFormatting.UNDERLINE + "Underlined";
         break;
      case 40:
         this.setting.setChromaName(!this.setting.isChromaName());
         button.displayString = (this.setting.isChromaName() ? TextFormatting.GREEN : TextFormatting.RED) + "Chroma name";
         break;
      case 120:
         this.setting.setChromaTimer(!this.setting.isChromaTimer());
         button.displayString = (this.setting.isChromaTimer() ? TextFormatting.GREEN : TextFormatting.RED) + "Chroma timer";
      }

   }

   private void updateSettings() {
      this.setting.setNameColor(this.sliderRed.GetValueAsInt() << 16 | this.sliderGreen.GetValueAsInt() << 8 | this.sliderBlue.GetValueAsInt());
      this.setting.setTimerColor(this.sliderRed2.GetValueAsInt() << 16 | this.sliderGreen2.GetValueAsInt() << 8 | this.sliderBlue2.GetValueAsInt());
   }

   public void onGuiClosed() {
      this.s.saveConfig();
   }

   public boolean doesGuiPauseGame() {
      return false;
   }
}
