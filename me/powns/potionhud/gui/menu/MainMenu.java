package me.powns.potionhud.gui.menu;

import java.io.IOException;
import java.util.Iterator;
import me.powns.potionhud.PotionHudMod;
import me.powns.potionhud.gui.components.GuiSlideControl;
import me.powns.potionhud.gui.components.GuiTransButton;
import me.powns.potionhud.settings.Settings;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

import net.minecraft.util.text.TextFormatting;
import org.lwjgl.opengl.GL11;

public class MainMenu extends GuiScreen {
   private PotionHudMod mod;
   private Settings s;
   private int lastX;
   private int lastY;
   private boolean dragging;
   private GuiSlideControl sliderAlpha;
   private GuiSlideControl sliderScale;

   public MainMenu(PotionHudMod mod, Settings s) {
      this.mod = mod;
      this.s = s;
   }

   public void initGui() {
      this.buttonList.add(new GuiTransButton(0, this.getCenter() - 75, this.getRowPos(3), 150, 20, "Edit effects"));
      this.buttonList.add(new GuiTransButton(5, this.getCenter() - 155, this.getRowPos(4), 150, 20, this.getBooleanString(this.s.isRomanNumerals(), "Roman numerals")));
      this.buttonList.add(new GuiTransButton(1, this.getCenter() - 155, this.getRowPos(5), 150, 20, this.getBooleanString(this.s.isRenderIcons(), "Render icons")));
      this.buttonList.add(new GuiTransButton(2, this.getCenter() + 5, this.getRowPos(4), 150, 20, this.getBooleanString(this.s.isRenderLevelsAbove4(), "Show effect levels above 4")));
      this.buttonList.add(new GuiTransButton(4, this.getCenter() + 5, this.getRowPos(5), 150, 20, this.getBooleanString(this.s.isShowWhileChat(), "Show in chat")));
      this.buttonList.add(this.sliderScale = new GuiSlideControl(3, this.getCenter() - 75, this.getRowPos(6), 150, 20, "Scale: ", 0.5F, 2.0F, this.s.getScale(), false));
   }

   public int getRowPos(int rowNumber) {
      return this.height / 4 + (24 * rowNumber - 24) - 16;
   }

   public int getCenter() {
      return this.width / 2;
   }

   public void drawScreen(int mouseX, int mouseY, float partialTicks) {
      GL11.glPushMatrix();
      super.drawDefaultBackground();
      GL11.glPushMatrix();
      GL11.glScaled(2.0D, 2.0D, 2.0D);
      this.drawCenteredString(this.mc.fontRenderer, "Powns' StatusEffect HUD", this.getCenter() / 2, 10, -1);
      GL11.glPopMatrix();
      this.updateButtons();
      if (this.dragging) {
         this.s.setAddX(this.s.getAddX() + (mouseX - this.lastX));
         this.s.setAddY(this.s.getAddY() + (mouseY - this.lastY));
      }

      this.lastX = mouseX;
      this.lastY = mouseY;
      this.s.setScale(this.sliderScale.GetValueAsFloat());
      super.drawScreen(mouseX, mouseY, partialTicks);
      GL11.glPopMatrix();
   }

   protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
      super.mouseClicked(mouseX, mouseY, mouseButton);
      if (!this.dragging && this.s.contains(mouseX, mouseY)) {
         this.dragging = true;
      }

   }

   protected void mouseReleased(int mouseX, int mouseY, int state) {
      super.mouseReleased(mouseX, mouseY, state);
      this.dragging = false;
   }

   protected void actionPerformed(GuiButton button) {
      switch(button.id) {
      case 0:
         this.mc.displayGuiScreen(new EditorMenu(this.mod, this.s));
         break;
      case 1:
         this.s.setRenderIcons(!this.s.isRenderIcons());
         button.displayString = this.getBooleanString(this.s.isRenderIcons(), "Render icons");
         break;
      case 2:
         this.s.setRenderLevelsAbove4(!this.s.isRenderLevelsAbove4());
         button.displayString = this.getBooleanString(this.s.isRenderLevelsAbove4(), "Show effect levels above 4");
      case 3:
      default:
         break;
      case 4:
         this.s.setShowWhileChat(!this.s.isShowWhileChat());
         button.displayString = this.getBooleanString(this.s.isShowWhileChat(), "Show in chat");
         break;
      case 5:
         this.s.setRomanNumerals(!this.s.isRomanNumerals());
         button.displayString = this.getBooleanString(this.s.isRomanNumerals(), "Roman numerals");
      }

   }

   public void onGuiClosed() {
      this.s.saveConfig();
   }

   public boolean doesGuiPauseGame() {
      return false;
   }

   private void updateButtons() {
      GuiButton b;
      for(Iterator var1 = this.buttonList.iterator(); var1.hasNext(); b.visible = !this.dragging) {
         b = (GuiButton)var1.next();
      }

      if (this.dragging) {
         this.drawGrid();
      }

   }

   private String getBooleanString(boolean bool, String buttonText) {
      TextFormatting add = bool ? TextFormatting.GREEN : TextFormatting.RED;
      return add + buttonText;
   }

   private void drawGrid() {
      this.drawHorizontalLine(3, this.width - 3, 3, -1);
      this.drawVerticalLine(3, 3, this.height - 3, -1);
      this.drawVerticalLine((int)((double)this.width * 0.33D), 0, this.height, -1);
      this.drawVerticalLine((int)((double)this.width * 0.67D), 0, this.height, -1);
      this.drawHorizontalLine(0, this.width, (int)((double)this.height * 0.33D), -1);
      this.drawHorizontalLine(0, this.width, (int)((double)this.height * 0.67D), -1);
      this.drawVerticalLine(this.width - 3, 3, this.height - 3, -1);
      this.drawHorizontalLine(3, this.width - 3, this.height - 3, -1);
   }
}
